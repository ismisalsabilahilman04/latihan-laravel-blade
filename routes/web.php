<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halaman.index');
});

Route::get('/hello', function () {
    return view('halaman.home');
});

Route::get('/form', function () {
    return view('halaman.form');
});

Route::get('/data-tables', function () {
    return view('table.data-tables');
});

Route::get('/table', function () {
    return view('table.table');
});